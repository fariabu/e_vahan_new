/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from "react";
import {
  StyleSheet,
  Image,
  Platform,
  BackHandler,
  DeviceEventEmitter,
  SafeAreaView,
  TextInput,
  Dimensions,
  FlatList,
  ActivityIndicator,
  TouchableOpacity,

  View,
  Text
} from "react-native";
import HomeRow from "./HomeRow";
import AsyncStorage from '@react-native-community/async-storage';
import LinearGradient from "react-native-linear-gradient";
import SearchBar from 'react-native-search-bar'
import StateRow from "./StateRow";
import MyLoader from "./MyLoader";
import {
  AdMobBanner,
  AdMobInterstitial,
  PublisherBanner,
  AdMobRewarded
} from "react-native-admob";
import MyConst from "./MyConst";
import MyBanner from "./MyBanner";

var SQLite = require('react-native-sqlite-storage')
export default class StatePetrol extends Component {
  constructor(props) {
    super(props);
   
    this.deviceWidth = Dimensions.get("window").width;
    this.response = "";
    this.state = {
      isLoading: true,
      data: [],
      completeData: [],
    };
  }
  bannerAd() {
    return (
      <MyBanner/>
    );
  }
  handleBackButton = () => {
    // DeviceEventEmitter.emit('Back',  {hello:"yes"});
    AdMobInterstitial.isReady((cb)=>{
      if(cb&&MyConst.isFullPage){
        AdMobInterstitial.showAd();
        AdMobInterstitial.requestAd();
      }
    });
    this.props.navigation.goBack();
    MyConst.CurrentScreen="Home";
    // this.props.navigation.goBack();
    return true;
  };
  componentWillUnmount() {
    if (Platform.OS == "android")
    
    BackHandler.removeEventListener(
      "hardwareBackPress",
      this.handleBackButton
    );
  }
  componentDidMount() {
    if (Platform.OS == "android")
    BackHandler.addEventListener("hardwareBackPress", this.handleBackButton);

    // AdMobInterstitial.setAdUnitID(MyConst.FullPageID);
    AdMobInterstitial.isReady((cb)=>{
    if(!cb){
      AdMobInterstitial.requestAd();
    }
  });
    let url = "https://mfapps.indiatimes.com/ET_Calculators/oilpricebycitystate.htm?type=city";
    fetch(url, {
      method: "GET"
    })
      .then(response => response.json())
      .then(responseJson => {

       
        this.setState({
          data:responseJson.results,
          completeData:responseJson.results,
           isLoading: false
         });

     
        

      })
      .catch(error => {
        console.error(error);
      });
  }
  _keyExtractor = (item, index) => item.name+""+index;

  _onPressItem = (item,isState,isRTO) => {
    MyConst.CITY=item.cityState;
    AsyncStorage.setItem(MyConst.cityID, MyConst.CITY);

    DeviceEventEmitter.emit('Refresh',  {hello:"yes"});
    this.props.navigation.goBack();
    
  };
  _renderItem = ({ item }) => (
    <StateRow item={item} onPressItem={this._onPressItem} isState={true} isRTO={false}/>
  );
  render() {
    let loader;
    if (this.state.isLoading)
      loader = (
        <View
          style={{ justifyContent: "center", width:'100%', height:'100%', position: "absolute" }}
        >
          <MyLoader text="Getting Cities.."/>
        </View>
      );
    return (
      <LinearGradient
        start={{ x: 0, y: 0 }}
        end={{ x: 0, y: 1 }}
        colors={["#FF9933", "white", "#138808"]}
        style={{ width: "100%", height: "100%" }}
      >
      <SafeAreaView style={styles.container}>
        {/* <View style={{ width: "100%", height: 40, backgroundColor: "gray" }} /> */}
        <View style={{flexDirection:"row",width,justifyContent:"center",alignItems:"center"}}>
        <TouchableOpacity
        style={{paddding: 10,}}
            onPress={() => {
              
              this.props.navigation.goBack();
              MyConst.CurrentScreen="Home";
            }}
          >
            <Image
              style={{
                width: 30,
                margin:10,
                height: 30,
                resizeMode: "contain",
                tintColor: "white",
                
               
              }}
              source={require("./assets/back.png")}
            />
          </TouchableOpacity>
          <TextInput
          
              style={{
                borderColor: "white",
                backgroundColor: "white",
                borderWidth: 1,
                marginTop:10,
               flex:1,
                padding:10,
                borderRadius: 5,
                marginLeft:10,
                marginRight: 10,
                fontSize: 20,
            
                textAlign: "center"
              }}
          
              onChangeText={text =>  {
                let items = this.state.completeData.filter(function(item){
                  return item.cityState.includes(text) ;
               });
               if(text!="")
               this.setState({data:items});
               else{
                this.setState({data:this.state.completeData});
               }
              }
              }
              maxLength={15}
              placeholder="Search City"
            
            />
        </View>
       
        {loader}
        
        <FlatList
          data={this.state.data}
          extraData={this.state}
          keyExtractor={this._keyExtractor}
          renderItem={this._renderItem}
        />
         {MyConst.isAdEnable ? this.bannerAd() : <View />}
      </SafeAreaView>
      </LinearGradient>
    );
  }
}
export const { width, height } = Dimensions.get("window");
const styles = StyleSheet.create({
  container: {
    flex: 1,

   
  },
  upper: {
    color: "gray",

    alignSelf: "center",
    textAlign: "center"
  }
});
