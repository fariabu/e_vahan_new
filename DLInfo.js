/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from "react";
import {
  StyleSheet,
  Image,
  SafeAreaView,
  
  Dimensions,
  Platform,
  BackHandler,
  TouchableOpacity,
  ActivityIndicator,
  View,
  Text
} from "react-native";
import LinearGradient from "react-native-linear-gradient";
import Geocoder from "react-native-geocoder";
import { TextInput, ScrollView } from "react-native-gesture-handler";
const DateDiff = require("date-diff");
import {
  AdMobBanner,
  AdMobInterstitial,
  PublisherBanner,
  AdMobRewarded
} from "react-native-admob";
import MyConst from "./MyConst";
import MyBanner from "./MyBanner";
export default class DLInfo extends Component {
  constructor(props) {
    super(props);

    this.deviceWidth = Dimensions.get("window").width;
    this.response = "";
    this.state = {
      location: "",
      // data: {
      //   id: "106",
      //   reg_prefix: "UP32HU",
      //   reg_suffix: "4583",
      //   reg_authority: "TRANSPORT NAGAR RTO LUCKNOW (UP32), UTTAR PRADESH",
      //   reg_date: "10-Apr-2017",
      //   chassis_no: "ME4JF507CHU0XXXXX",
      //   engine_no: "JF50EU50XXXXX",
      //   owner_name: "ABU FARHAN",
      //   vehicle_class: "M-CYCLE/SCOOTER (2WN)",
      //   fuel_type: "PETROL",
      //   maker_model: "HONDA MOTORCYCLE AND SCOOTER INDIA (P) LTD / ACTIVA 4G",
      //   fitness_upto: "09-Apr-2032",
      //   insurance_upto: "19-Mar-2019",
      //   fuel_norms: "BHARAT STAGE IV",
      //   road_tax_upto: "",
      //   noc: null,
      //   created_at: "2019-01-23 18:49:58",
      //   updated_at: "2019-01-23 18:49:58"
      // }
      data: this.props.navigation.getParam("data", "")
    };
  }

  handleBackButton = () => {
    // DeviceEventEmitter.emit('Back',  {hello:"yes"});
    AdMobInterstitial.isReady((cb)=>{
      if(cb&&MyConst.isFullPage){
        AdMobInterstitial.showAd();
        AdMobInterstitial.requestAd();
      }
    });
    this.props.navigation.goBack();
    MyConst.CurrentScreen="DLInfo";
    // this.props.navigation.goBack();
    return true;
  };
  componentWillUnmount() {
    if (Platform.OS == "android")
    
    BackHandler.removeEventListener(
      "hardwareBackPress",
      this.handleBackButton
    );
  }
  componentDidMount() {
    if (Platform.OS == "android")
    BackHandler.addEventListener("hardwareBackPress", this.handleBackButton);
    // AdMobInterstitial.setAdUnitID(MyConst.FullPageID);
    AdMobInterstitial.isReady((cb)=>{
    if(!cb){
      AdMobInterstitial.requestAd();
    }
  });
    this.setState({ data: this.props.navigation.getParam("data", "") }, function() {
     
    });
  }
  getWidth(toCalculate) {
    return (toCalculate / 375) * width;
  }
  bannerAd() {
    return (
      <MyBanner/>
    );
  }
  smartBannerAd() {
    return (
      <View
        style={{
          justifyContent: "center",
          marginTop: 10,
        
          alignItems: "center"
        }}
      >
        <AdMobBanner adSize="smartBannerPortrait" adUnitID={MyConst.BannerId} />
      </View>
    );
  }
  render() {

    const gradient = `linear-gradient(-225deg, #231557 0%, #44107A 29%, #FF1361 67%, #FFF800 100%), repeating-linear-gradient(-115deg, transparent, transparent 20px, rgba(255,255,255,0.1) 20px, rgba(255,255,255,0.1) 40px), repeating-linear-gradient(115deg, transparent, transparent 20px, rgba(255,255,255,0.1) 20px, rgba(255,255,255,0.1) 40px)`;
    let date = new Date(this.state.data.reg_date);
    let date1 = new Date();
    var diff = new DateDiff(date1, date);
    let age =
      diff
        .years()
        .toString()
        .split(".")[0] +
      " year and " +
      (diff.months() % 12).toString().split(".")[0] +
      " months"; // ===> 1.9;
    console.log(age);
    return (
      <LinearGradient
        start={{ x: 0, y: 0 }}
        end={{ x: 0, y: 1 }}
        colors={["#FF9933", "white", "#138808"]}
        style={{ width: "100%", height: "100%" }}
      >
        <SafeAreaView
          style={{
            backgroundColor: "#0004",
            width: "100%",
            height: "100%",
            shadowColor: "#000",
            shadowOffset: { width: 0, height: 2 },
            shadowOpacity: 0.8,

            elevation: 1
          }}
        >
        <TouchableOpacity style={{paddding: 10,}} onPress={()=>{
          
          this.props.navigation.goBack()}}>
          <Image
            style={{
              width: 30,
              height: 30,
              resizeMode: "contain",
              tintColor: "white",
              margin:10,
            }}
            source={require("./assets/back.png")}
          />
          </TouchableOpacity>
          <ScrollView>
            <View>
              <Image
                source={require("./assets/car.png")}
                style={{ width: "80%", height: 200, alignSelf: "center" }}
                resizeMode="contain"
              />

              <View
                style={{
                  width: this.getWidth(340),
                  borderRadius: 5,
                  alignSelf: "center",
                  flexDirection: "column",
                  backgroundColor: "white"
                }}
              >
                <Text style={styles.headingText}>Name</Text>

                <Text style={styles.mainText}>
                  {this.state.data.name}
                </Text>
              </View>
              <View
                style={{
                  width: this.getWidth(340),
                  borderRadius: 5,
                  marginTop: 10,
                  alignSelf: "center",
                  flexDirection: "column",
                  backgroundColor: "white"
                }}
              >
                <Text style={styles.headingText}>DL No</Text>

                <Text style={styles.mainText}>
                  {this.state.data.dl_no}
                </Text>
              </View>
              <View
                style={{
                  width: this.getWidth(340),
                  borderRadius: 5,
                  marginTop: 10,
                  alignSelf: "center",
                  flexDirection: "column",
                  backgroundColor: "white"
                }}
              >
                <Text style={styles.headingText}>Date of Issue</Text>

                <Text style={styles.mainText}>
                  {this.state.data.date_of_issue}
                </Text>
              </View>
              <View
                style={{
                  width: this.getWidth(340),
                  borderRadius: 5,
                  marginTop: 10,
                  alignSelf: "center",
                  flexDirection: "column",
                  backgroundColor: "white"
                }}
              >
                <Text style={styles.headingText}>
                  RTO Office
                </Text>

                <Text style={styles.mainText}>
                  {this.state.data.last_transaction_at}
                </Text>
              </View>
              {MyConst.isAdEnable ? this.smartBannerAd() : <View />}
              <View
                style={{
                  width: this.getWidth(340),
                  borderRadius: 5,
                  marginTop: 10,
                  alignSelf: "center",
                  flexDirection: "column",
                  backgroundColor: "white"
                }}
              >
                <Text style={styles.headingText}>
                  Expiry Date
                </Text>

                <Text style={styles.mainText}>{this.state.data.non_transport_to}</Text>
              </View>
              <View
                style={{
                  width: this.getWidth(340),
                  borderRadius: 5,
                  marginTop: 10,
                  alignSelf: "center",
                  flexDirection: "column",
                  backgroundColor: "white"
                }}
              >
                <Text style={styles.headingText}>Source</Text>

                <Text style={styles.mainText}>
                  {this.state.data.source}
                </Text>
              </View>

              <View
                style={{
                  width: this.getWidth(340),
                  borderRadius: 5,
                  marginTop: 10,
                  alignSelf: "center",
                  flexDirection: "column",
                  backgroundColor: "white"
                }}
              >
                <Text style={styles.headingText}>Status</Text>

                <Text style={styles.mainText}>{this.state.data.status}</Text>
              </View>
            
            </View>
          </ScrollView>
          {MyConst.isAdEnable ? this.bannerAd() : <View />}
        </SafeAreaView>
      </LinearGradient>
    );
  }
}
export const { width, height } = Dimensions.get("window");
const styles = StyleSheet.create({
  container: {
    flex: 1,

    backgroundColor: "black"
  },
  bg: {},
  headingText: {
    color: "#FF9933",
    fontWeight: "bold",
    padding: 10,
    fontSize: 14
  },
  mainText: {
    color: "#138808",
    paddingLeft: 10,
    paddingRight: 10,
    width: "100%",
    paddingBottom: 10,

    fontSize: 16
  },
  upper: {
    color: "gray",

    alignSelf: "center",
    textAlign: "center"
  }
});
