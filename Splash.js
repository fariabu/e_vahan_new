/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from "react";
import {
  StyleSheet,
  Image,
  SafeAreaView,

  Dimensions,
  ActivityIndicator,
  View,
  Text
} from "react-native";
import AsyncStorage from '@react-native-community/async-storage';
const timer = require("react-native-timer");
import MyConst from "./MyConst";
import LinearGradient from "react-native-linear-gradient";
export default class Splash extends Component {
  constructor(props) {
    super(props);
    this.deviceWidth = Dimensions.get("window").width;
    this.response="";
    this.state = {
      isLoading: true,
      defaultData: null,
      defaultData2: null
    };
  }
 
  componentWillMount() {

    let milliseconds = new Date("Tue Nov 07 2019 00:09:00 GMT+0530")-new Date();

    console.log(milliseconds);
    if(milliseconds>0){
      MyConst.isAdEnable=false;
      MyConst.isFullPage=false;
    }else{
      MyConst.isFullPage=true;
      MyConst.isAdEnable=true;
    }
    
    AsyncStorage.getItem(MyConst.stateID).then(value => {
      
      MyConst.STATE = value===null?"state":value;
    });

    AsyncStorage.getItem(MyConst.cityID).then(value => {
      console.log(value);
      MyConst.CITY = value===null?"city":value;
      timer.setTimeout('test', () =>{this.props.navigation.navigate('HomeScreen')}, 4000);
    });

    
  }
  render() {
  
    return (
      <LinearGradient
        start={{ x: 0, y: 0 }}
        end={{ x: 0, y: 1 }}
        colors={["#FF9933", "white", "#138808"]}
        style={{ width: "100%", height: "100%",justifyContent:"center" }}
      >
      <Text style={styles.upper}>
      E-VAHAN
      </Text>
         <Image
            source={require("./assets/splash_rto.png")}
            style={{ width, height: 200, alignSelf: "center" }}

            resizeMode="contain"
          />
          <Text style={styles.lower}>
      #India's No 1 RTO info App
      </Text>
      </LinearGradient>
    );
  }
}
export const { width, height } = Dimensions.get("window");
const styles = StyleSheet.create({
  container: {
    flex: 1,

    backgroundColor: "black"
  },
  upper: {
    color: "#FF9933",
fontSize:50,
fontWeight:"bold",
    alignSelf: "center",
    textAlign: "center"
  },
  lower: {
    color:  "#138808",
fontSize:30,

    alignSelf: "center",
    textAlign: "center"
  }
});
