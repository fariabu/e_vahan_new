/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from "react";
import {
  StyleSheet,
  Image,
  SafeAreaView,
  ImageBackground,
  Dimensions,
  TouchableOpacity,
  ActivityIndicator,
  View,
  Text
} from "react-native";
import LinearGradient from "react-native-linear-gradient";
import Geocoder from "react-native-geocoder";
import { TextInput, FlatList } from "react-native-gesture-handler";
export default class StateRTO extends Component {
  constructor(props) {
    super(props);

  
    this.state = {
      isLoading: false,
      data: "",
     
    };
  }
  refresh() {
    fetch(
      "https://mfapps.indiatimes.com/ET_Calculators/oilpricebycitystate.htm?type=state",
      {
        method: "GET"
      }
    )
      .then(response => response.json())
      .then(responseJson => {
        let items = responseJson.results;
        for (var i = 0; i < items.length; i++) {
          if (items[i].cityState == this.state.state) {
            this.setState({
              Petrol: items[i].petrolPrice.substring(0, 5),
              Diesel: items[i].dieselPrice.substring(0, 5)
            });
          }
        }
      })
      .catch(error => {
        // console.error(error);
      });

    // console.log("here");
  }
  componentDidMount() {

   
  }
  getWidth(toCalculate) {
    return (toCalculate / 375) * width;
  }

  render() {
    const gradient = `linear-gradient(-225deg, #231557 0%, #44107A 29%, #FF1361 67%, #FFF800 100%), repeating-linear-gradient(-115deg, transparent, transparent 20px, rgba(255,255,255,0.1) 20px, rgba(255,255,255,0.1) 40px), repeating-linear-gradient(115deg, transparent, transparent 20px, rgba(255,255,255,0.1) 20px, rgba(255,255,255,0.1) 40px)`;

    return (
      <LinearGradient
        start={{ x: 0, y: 0 }}
        end={{ x: 0, y: 1 }}
        colors={["#FF9933", "white", "#138808"]}
        style={{ width: "100%", height: "100%" }}
      >
        <SafeAreaView
          style={{
            backgroundColor: "#0004",
            width: "100%",
            height: "100%",
            shadowColor: "#000",
            shadowOffset: { width: 0, height: 2 },
            shadowOpacity: 0.8,

            elevation: 1
          }}
        >
         {/* <FlatList */}
        </SafeAreaView>
      </LinearGradient>
    );
  }
}
export const { width, height } = Dimensions.get("window");
const styles = StyleSheet.create({
  container: {
    flex: 1,

    backgroundColor: "black"
  },
  text: {
    color: "white",
    textAlign: "center",
    fontWeight: "bold",
    alignSelf: "center",
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,

    elevation: 1,
    fontSize: 25
  },
  rate: {
    color: "black",
    textAlign: "center",
    fontWeight: "bold",
    alignSelf: "center",

    fontSize: 25
  },
  upper: {
    color: "gray",

    alignSelf: "center",
    textAlign: "center"
  }
});
