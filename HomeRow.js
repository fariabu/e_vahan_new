import React, { PureComponent } from "react";
import {
  StyleSheet,
  Text,
  
  View,
 
  Dimensions,
  TouchableWithoutFeedback,
  
} from "react-native";

export default class HomeRow extends PureComponent {
  _onPress = () => {
   this.props.onPressItme(this.props.id);
  };
  getWidth(toCalculate) {
    return (toCalculate / 375) * width;
  }
  render() {
    return (
      <TouchableWithoutFeedback onPress={this._onPress} >
        <View style={{backgroundColor:'white', margin:10,borderRadius:5}}>
          <View style={{flexDirection:'column',padding:5}}>
            <Text>{this.props.item.name}</Text>
            <Text>{this.props.item.email}</Text>
          </View>
          </View>
      </TouchableWithoutFeedback>
    );
  }
}
export const { width, height } = Dimensions.get("window");
const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    backgroundColor: "#1693e5"
  },
  upper: {
    color: "gray",

    alignSelf: "center",
    textAlign: "center"
  },
  text: {
    fontSize: 15
  },
  colorAccent: { color: "#8dbb3c" }
});
