import sha1 from 'crypto-js/sha1';
import moment from 'moment';
import {
    decrypt,
    encrypt,
    get_model,
    get_os_version,
    hide_last_5_chars,
    randomInteger,
    sanitizeText,
    send_raw_data,
} from './rto_utils';


async function fetch_from_api(vehicle_number) {
    const key = "mParivahan#2019@";

    let base_uri = 'https://mparivahan.parivahan.gov.in/api/';

    let imei = (randomInteger(100000000, 1000000000)).toString();
    let device_id = sha1(randomInteger(1, 100)).toString();
    let model = get_model();

    let os_version = get_os_version();

    let device_token = sha1(randomInteger(0, 100));

    const uri = base_uri + 'vt-enc-rc';

    let body = new FormData();
    body.append('d_model', await encrypt(model, key));
    body.append('virtual_rc', await encrypt('2', key));
    body.append('d_imei_number', imei);
    body.append('device_id', await encrypt(device_id, key));
    body.append('d_os_version', await encrypt(os_version, key));
    body.append('d_os_type', await encrypt('ANDROID', key));
    body.append('doc_number', await encrypt(vehicle_number, key));
    body.append('doc_type', await encrypt('1', key));
    body.append('d_token', await encrypt(device_token, key));

    let response = await fetch(uri, {method: 'POST', body: body, credentials: 'include'});
    let text = await response.text();
    // send_raw_data(text, 'rc');
    return await decrypt(text, key);
}


export async function fetch_rc_details(vehicle_number){
    let num = sanitizeText(vehicle_number);
    let response = await fetch_from_api(num);
    let json = await JSON.parse(response);

    let now =  moment().format("YYYY-MM-DD HH:mm:ss");

    let len = num.length;
    let reg_prefix = num.substr(0, len / 2);
    let reg_suffix = num.substr(len / 2, len);


    let res = json.result;

    return {
        reg_prefix: reg_prefix,
        reg_suffix: reg_suffix,
        reg_authority: res.rc_registered_at,
        reg_date: res.rc_regn_dt,
        chassis_no: hide_last_5_chars(res.rc_chasi_no),
        engine_no: hide_last_5_chars(res.rc_eng_no),
        owner_name: res.rc_owner_name,
        vehicle_class: res.rc_vh_class_desc,
        fuel_type: res.rc_fuel_desc,
        maker_model: `${res.rc_maker_desc} / ${res.rc_maker_model}`,
        fitness_upto: res.rc_fit_upto,
        insurance_upto: res.rc_insurance_upto,
        fuel_norms: res.rc_norms_desc,
        road_tax_upto: res.rc_tax_upto,
        noc: res.rc_noc_details,
        color: res.rc_color,
        owner_count: res.rc_owner_sr,
        created_at: now,
        updated_at: now,
    };

}



