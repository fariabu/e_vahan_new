/** @format */
import { Platform } from "react-native";
class MyConst {
  /*===================KEY===============*/
  static stateID = "stateID";
  static cityID = "cityyID";
  static CurrentScreen="";
static isLoaded=false;
  static BannerId =
    Platform.OS == "android"
      ? "ca-app-pub-5733573208022971/2076653177"
      : "ca-app-pub-5733573208022971/6602931989";
      // "ca-app-pub-3940256099942544/6300978111";
  static FullPageID =
    Platform.OS == "android"
      ? "ca-app-pub-5733573208022971/6562693092"
      : "ca-app-pub-5733573208022971/8653422398";
    // "ca-app-pub-3940256099942544/1033173712";

  static isAdEnable = true;
  static isFullPage = true;

  /*=================Variable====================*/
  static STATE = "state";
  static CITY = "city";
}
module.exports = MyConst;
