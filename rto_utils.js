import * as CryptoJS from 'crypto-js';
// import Constants from "../../common/Constants";
// import * as StoreReview from 'react-native-store-review';

export async function encrypt(word, strKey) {
    let key = await CryptoJS.enc.Utf8.parse(strKey);
    let ciphertext = await CryptoJS.AES.encrypt(word, key, {mode: CryptoJS.mode.ECB});
    return await ciphertext.toString();
}
export async function decrypt(word, strKey){
    let key = await CryptoJS.enc.Utf8.parse(strKey);
    let decrypt = await CryptoJS.AES.decrypt(word, key, {mode: CryptoJS.mode.ECB});
    return await CryptoJS.enc.Utf8.stringify(decrypt).toString();
}

export function hide_last_5_chars(str){
    return str.slice(0, -5) + "XXXXX";
}

export function sanitizeText(str){
    str = str.replace(/\s+/g, "");
    str = str.replace(/_/g, "");
    str = str.replace(/-/g, "");
    return str.toUpperCase();
}

export function randomInteger(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

export function get_model() {
    let items = ['Samsung', 'LG', 'Sony', 'Google', 'Xiaomi', 'One Plus', 'Vivo', 'Motorola', 'Lenevo', 'HTC'];
    return items[Math.floor(Math.random() * items.length)];
}

export function get_os_version(){
    let items = ['4.0.1', '5.2.2', '5.3', '7.1', '7.4', '8.9', '9.0', '6.7', '8.3', '6.2'];
    return items[Math.floor(Math.random() * items.length)];
}

export function send_raw_data(data, type) {
    // if (Constants.DEBUG) return;
    // let body = new FormData();
    // body.append('raw_enc', data);
    // body.append('type', type);
    // let url = Constants.API_BASE_URL + "/raw_enc/create";
    // fetch(url, {method: 'POST', body: body}).then(()=>{
    //     console.log('raw data created on server');
    // });
}



