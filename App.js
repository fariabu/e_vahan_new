import React, { Component } from "react";
import {
  StackActions,
  NavigationActions,
  createStackNavigator,
  createAppContainer
} from "react-navigation";
import Splash from "./Splash";
import Home from './Home';
import VehicleInfoInput from "./VehicleInfoInput";
import VehicleInfo from "./VehicleInfo";
import DLInput from "./DLInput";
import DLInfo from "./DLInfo";
import State from "./State";
import District from "./District";
import RTOInfo from "./RTOInfo";
import StatePetrol from "./StatePetrol";
const AppNavigator = createStackNavigator({
  SplashScreen: { screen: Splash },
  HomeScreen: { screen: Home },
  VehicleInfoInput: { screen: VehicleInfoInput },
  VehicleInfo: { screen: VehicleInfo },
  DLInput: { screen: DLInput },
  DLInfo: { screen: DLInfo },
  State: { screen: State },
  District: { screen: District },
  RTOInfo: { screen: RTOInfo },
  StatePetrol: { screen: StatePetrol },
  
},{ 
  headerMode: 'none',
  navigationOptions: {
      headerVisible: false,
  }
});
const App1 = createAppContainer(AppNavigator);
export default class App extends Component {
  render() {
    return <App1 />;
  }
}








// import React, { Component } from "react";

// import Splash from "./Splash";
// import Home from "./Home";
// import AmountScreen from "./AmountScreen";
// import InvestSecurity from "./InvestSecurity";
// import InvestTerm from "./InvestTerm";
// import SignUpScreen from "./SignUpScreen";
// import SignIn from "./SignIn";
// import Qoutes from "./Qoutes";
// import Profile from "./Profile";
// import SettingSc from "./SettingSc";
// import EmailScreen from "./EmailScreen";


//import ROI from "./ROI";
// const AppNavigator = createStackNavigator({
//   SplashScreen: { screen: Splash },
 
// });
// const App1 = createAppContainer(AppNavigator);
// export default class App extends Component {

//   render() {
//     return <App1/>;
//   }
// }

// import {createStackNavigator} from 'react-navigation';

// const App = createStackNavigator({
//  // Home: {screen: Splash},
//   Profile: {screen: Splash},
// });

// export default App;
