/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from "react";
import {
  StyleSheet,
  Image,
  SafeAreaView,
  BackHandler,
  Linking,
  ImageBackground,
  DeviceEventEmitter,
  Dimensions,
  Platform,
  TouchableOpacity,
  ActivityIndicator,
  View,
  Text
} from "react-native";
import LinearGradient from "react-native-linear-gradient";
import {
  AdMobBanner,
  AdMobInterstitial,
  PublisherBanner,
  AdMobRewarded
} from "react-native-admob";
import MyConst from "./MyConst";
import { PermissionsAndroid } from "react-native";
import Geocoder from "react-native-geocoder";

import { TextInput, ScrollView } from "react-native-gesture-handler";
import MyBanner from "./MyBanner";
export default class Home extends Component {
  constructor(props) {
    super(props);

    this.deviceWidth = Dimensions.get("window").width;
    this.response = "";
    this.state = {
      location: "",

      Petrol: "XX.XX",
      Diesel: "XX.XX"
    };
  }
  bannerAd() {
    return (
      <MyBanner/>
    );
  }
  refresh() {
    fetch(
      "https://mfapps.indiatimes.com/ET_Calculators/oilpricebycitystate.htm?type=city",
      {
        method: "GET"
      }
    )
      .then(response => response.json())
      .then(responseJson => {
        let items = responseJson.results;
        let P = "";
        let D = "";
        let isFound = false;
        for (var i = 0; i < items.length; i++) {
          if (items[i].cityState == MyConst.CITY) {
            isFound = true;
            this.setState({
              Petrol: items[i].petrolPrice.substring(0, 5),
              Diesel: items[i].dieselPrice.substring(0, 5)
            });
          }
          if (items[i].cityState == "Delhi") {
            (P = items[i].petrolPrice.substring(0, 5)),
              (D = items[i].dieselPrice.substring(0, 5));
          }
        }
        if (!isFound) {
          MyConst.CITY = "Delhi";
          // this.setState({ location: MyConst.CITY });
          this.setState({
            Petrol: P,
            Diesel: D,
            location: MyConst.CITY
          });
        }
      })
      .catch(error => {
        // console.error(error);
      });

    // console.log("here");
  }
  refreshState() {
    fetch(
      "https://mfapps.indiatimes.com/ET_Calculators/oilpricebycitystate.htm?type=state",
      {
        method: "GET"
      }
    )
      .then(response => response.json())
      .then(responseJson => {
        let items = responseJson.results;
        for (var i = 0; i < items.length; i++) {
          if (items[i].cityState == MyConst.STATE) {
            this.setState({
              Petrol: items[i].petrolPrice.substring(0, 5),
              Diesel: items[i].dieselPrice.substring(0, 5)
            });
          }
        }
      })
      .catch(error => {
        // console.error(error);
      });

    // console.log("here");
  }

  addBackEvent() {
    if (Platform.OS == "android")
      BackHandler.addEventListener("hardwareBackPress", this.handleBackButton);
  }
  removeEvent() {
    
  }
  componentWillUnmount() {
    if (Platform.OS == "android")
    BackHandler.removeEventListener(
      "hardwareBackPress",
      this.handleBackButton
    );
  }
  componentWillMount() {
if(Platform.OS=="android"||((new Date("Tue Nov 07 2019 00:09:00 GMT+0530")-new Date())<0)){
  MyConst.isFullPage=true;
}else MyConst.isFullPage=false;
// let 
    DeviceEventEmitter.addListener("Refresh", e => {
      // this.addBackEvent();
      MyConst.CurrentScreen="Home";
      this.setState({ location: MyConst.CITY });
      this.refresh();
    });
    DeviceEventEmitter.addListener("Back", e => {
      // this.addBackEvent();
    });
  }

  handleBackButton = () => {
    if( MyConst.CurrentScreen=="Home")
    BackHandler.exitApp();
    return true;
  };
  async requestLocationPermission() {
    this.addBackEvent();
    this.setState({ location: MyConst.CITY });
        this.refresh();
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          title: "Location Permission",
          message: "This app needs access to your location"
        }
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        this.getLocation();
      } else {
        this.setState({ location: MyConst.CITY });
        this.refresh();
      }
    } catch (err) {
      this.setState({ location: MyConst.CITY });
      this.refresh();
    }
  }
  componentDidMount() {
    AdMobInterstitial.setAdUnitID(MyConst.FullPageID);
    AdMobInterstitial.isReady(cb => {
      if (!cb) {
        AdMobInterstitial.requestAd();
      }
    });
    Platform.OS == "android"
      ? this.requestLocationPermission()
      : this.getLocation();
  }

  getLocation() {
    this.watchID = navigator.geolocation.watchPosition(
      position => {
        // Create the object to update this.state.mapRegion through the onRegionChange function
        latLong = {
          // lat: 26.83928 /*position.coords.latitude,*/,
          // lng: 80.92313
          lat: position.coords.latitude,
          lng: position.coords.longitude
        };
      
        if (MyConst.CITY == "city") {
          Geocoder.geocodePosition(latLong).then(res => {
            console.log(res);

            MyConst.STATE = res[0].adminArea;
            MyConst.CITY = res[0].locality;
            this.setState({ location: MyConst.CITY });
            this.refresh();
          });
        } else {
          this.setState({ location: MyConst.CITY });
          this.refresh();
        }
      },
      error => {
        this.setState({ location: MyConst.CITY });
        this.refresh();
      }
    );
  }
  getWidth(toCalculate) {
    return (toCalculate / 375) * width;
  }

  render() {
    const gradient = `linear-gradient(-225deg, #231557 0%, #44107A 29%, #FF1361 67%, #FFF800 100%), repeating-linear-gradient(-115deg, transparent, transparent 20px, rgba(255,255,255,0.1) 20px, rgba(255,255,255,0.1) 40px), repeating-linear-gradient(115deg, transparent, transparent 20px, rgba(255,255,255,0.1) 20px, rgba(255,255,255,0.1) 40px)`;

    return (
      <LinearGradient
        start={{ x: 0, y: 0 }}
        end={{ x: 0, y: 1 }}
        colors={["#FF9933", "white", "#138808"]}
        style={{ width: "100%", height: "100%" }}
      >
        <SafeAreaView
          style={{
            backgroundColor: "#0004",
            width: "100%",
            height: "100%"
          }}
        >
          <ScrollView>
            <View
              style={{
                padding: this.getWidth(20),
                borderRadius: 10,
                marginTop: 10,
                alignSelf: "center",
                width: width - this.getWidth(40),
                backgroundColor: "white"
              }}
            >
              <Image
                source={require("./assets/loader3.gif")}
                style={{
                  width: width - this.getWidth(40),
                  height: 120,
                  alignSelf: "center"
                }}
                resizeMode="cover"
              />
              <Text style={styles.upper}>E-VAHAN</Text>
            </View>
            <View
              style={{
                width,
                padding: this.getWidth(20),
                shadowColor: "#000",
                shadowOffset: { width: 0, height: 1 },
                shadowOpacity: 0.8,

                elevation: 6
              }}
            >
              <View
                style={{
                  justifyContent: "space-between",
                  flexDirection: "row"
                }}
              >
                <TouchableOpacity
                  style={{
                    width: this.getWidth(150),
                    height: 100,
                    justifyContent: "center",
                    padding: 10,
                    backgroundColor: "white",

                    borderRadius: 10
                  }}
                  onPress={() => {
                    this.removeEvent();
                    MyConst.CurrentScreen="VehicleInfoInput";
                    this.props.navigation.navigate("VehicleInfoInput");
                  }}
                >
                  <Image
                    source={require("./assets/search.png")}
                    style={{
                      width: "100%",
                      height: "60%",
                      alignSelf: "center",
                      tintColor: "#0004",
                      justifyContent: "center"
                    }}
                    resizeMode="contain"
                  />
                  <Text style={styles.text}>Find RC</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={{
                    width: this.getWidth(150),
                    height: 100,

                    alignSelf: "center",
                    justifyContent: "center",
                    padding: 10,
                    backgroundColor: "white",
                    borderRadius: 10
                  }}
                  onPress={() => {
                    MyConst.CurrentScreen="State";
                    this.props.navigation.navigate("State");
                    this.removeEvent();
                  }}
                >
                  <Image
                    source={require("./assets/placeholder.png")}
                    style={{
                      width: "100%",
                      height: "60%",
                      alignSelf: "center",
                      tintColor: "#0004",
                      justifyContent: "center"
                    }}
                    resizeMode="contain"
                  />
                  <Text style={styles.text}>Find RTO</Text>
                </TouchableOpacity>
              </View>

              <View
                style={{
                  justifyContent: "space-between",
                  flexDirection: "row"
                }}
              >
                <TouchableOpacity
                  style={{
                    width: this.getWidth(150),
                    height: 100,
                    marginTop: 10,
                    alignSelf: "center",
                    justifyContent: "center",
                    padding: 10,
                    backgroundColor: "white",
                    borderRadius: 10
                  }}
                  onPress={() => {
                    MyConst.CurrentScreen="DLINput";
                    this.props.navigation.navigate("DLInput");
                    this.removeEvent();
                  }}
                >
                  <Image
                    source={require("./assets/contact.png")}
                    style={{
                      width: "100%",
                      height: "60%",
                      alignSelf: "center",
                      tintColor: "#0004",
                      justifyContent: "center"
                    }}
                    resizeMode="contain"
                  />
                  <Text style={styles.text}>Find DL</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={{
                    width: this.getWidth(150),
                    height: 100,
                    marginTop: 10,
                    alignSelf: "center",
                    justifyContent: "center",
                    padding: 10,
                    backgroundColor: "white",
                    borderRadius: 10
                  }}
                  onPress={() => {
                    Linking.openURL( Platform.OS=="android"?
                      "https://play.google.com/store/apps/details?id=com.rto.carinfo.e_vahan":"https://itunes.apple.com/app/id1451220596"
                    );
                  }}
                >
                  <Image
                    source={require("./assets/rating.png")}
                    style={{
                      width: "100%",
                      height: "60%",
                      alignSelf: "center",
                      tintColor: "#0004",
                      justifyContent: "center"
                    }}
                    resizeMode="contain"
                  />
                  <Text style={styles.text}>Rate US</Text>
                </TouchableOpacity>
              </View>
              <View
                style={{
                  width: width - this.getWidth(40),

                  marginTop: 10,
                  marginLeft: this.getWidth(20),
                  marginRight: this.getWidth(20),
                  alignSelf: "center",
                  padding: 10,
                  backgroundColor: "white",
                  borderRadius: 10
                }}
              >
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "center",
                    width: "100%"
                  }}
                >
                  <Text
                    style={{
                      alignSelf: "center",
                      height: "100%",
                      textAlign: "center",

                      fontSize: 16,
                      paddingBottom: 5,
                      color: "black",
                      fontWeight: "bold"
                    }}
                    
                    
                  >
                    Today's Fuel Price
                  </Text>
                </View>

                <View style={{ height: 1, backgroundColor: "#0002" }} />
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-between"
                  }}
                >
                  <View style={{ width: "45%", justifyContent: "center" }}>
                    <Text style={styles.rate}>{"₹" + this.state.Petrol}</Text>
                    <Text
                      style={{
                        alignSelf: "center",
                        padding: 5,
                        fontSize: 16,
                        color: "#427d31"
                      }}
                    >
                      PETROL
                    </Text>
                  </View>
                  <View
                    style={{ height: 90, width: 1, backgroundColor: "#0002" }}
                  />
                  <View style={{ width: "45%", justifyContent: "center" }}>
                    <Text style={styles.rate}>{"₹" + this.state.Diesel}</Text>
                    <Text
                      style={{
                        alignSelf: "center",
                        padding: 5,
                        fontSize: 16,
                        color: "#3053a9"
                      }}
                    >
                      DIESEL
                    </Text>
                  </View>
                </View>
                <View style={{ height: 1, backgroundColor: "#0002" }} />
                <View
                  style={{
                    flexDirection: "row",

                    width: "100%"
                  }}
                >
                  <View
                    style={{
                      width: "100%",
                      position: "absolute",
                      justifyContent: "flex-end",
                      alignItems: "center",
                      padding: 5
                    }}
                  >
                    <Text
                      style={{
                        alignSelf: "flex-end",
                        height: "100%",
                        textAlign: "center",

                        fontSize: 16,
                        color: "#abab11",
                        fontWeight: "bold"
                      }}
                      onPress={() => {
                        AdMobInterstitial.isReady(cb => {
                          if (cb&&MyConst.isFullPage) {
                            AdMobInterstitial.showAd();
                            AdMobInterstitial.requestAd();
                          }
                        });
                        MyConst.CurrentScreen="ChangeCity";
                        this.props.navigation.navigate("StatePetrol");
                      }}
                    >
                      Change City
                    </Text>
                  </View>

                  <Image
                    source={require("./assets/gas.png")}
                    style={{
                      width: 25,
                      height: 40
                    }}
                    resizeMode="contain"
                  />
                  <Text
                    style={{ alignSelf: "center", padding: 5, fontSize: 14 }}
                  >
                    {this.state.location}
                  </Text>
                </View>
              </View>
            </View>
          </ScrollView>
          {MyConst.isAdEnable ? this.bannerAd() : <View />}
          {/* <Text style={{color:"white"}}>{this.state.location}</Text> */}
        </SafeAreaView>
      </LinearGradient>
    );
  }
}
export const { width, height } = Dimensions.get("window");
const styles = StyleSheet.create({
  container: {
    flex: 1,

    backgroundColor: "black"
  },
  text: {
    color: "gray",
    textAlign: "center",
    fontWeight: "bold",
    alignSelf: "center",

    fontSize: 25
  },
  rate: {
    color: "black",
    textAlign: "center",
    fontWeight: "bold",
    alignSelf: "center",

    fontSize: 25
  },
  upper: {
    color: "#FF9933",
    position: "absolute",
    fontSize: 30,
    fontWeight: "bold",

    alignSelf: "center",
    textAlign: "center"
  }
});
