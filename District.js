/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from "react";
import {
  StyleSheet,
  Image,
  SafeAreaView,
  TextInput,
  Dimensions,
  FlatList,
  ActivityIndicator,
  View,
  TouchableOpacity,
  Text
} from "react-native";
import HomeRow from "./HomeRow";
import {
  AdMobBanner,
  AdMobInterstitial,
  PublisherBanner,
  AdMobRewarded
} from "react-native-admob";
import MyConst from "./MyConst";
import LinearGradient from "react-native-linear-gradient";
import SearchBar from 'react-native-search-bar'
import StateRow from "./StateRow";
import MyLoader from "./MyLoader";
import MyBanner from "./MyBanner";
var SQLite = require('react-native-sqlite-storage')
export default class District extends Component {
  constructor(props) {
    super(props);
   
    this.deviceWidth = Dimensions.get("window").width;
    this.response = "";
    this.state = {
      isLoading: true,
      data: [],
      completeData: [],
    };
  }
  bannerAd() {
    return (
      <MyBanner/>
    );
  }
  componentDidMount() {

    // AdMobInterstitial.setAdUnitID(MyConst.FullPageID);
    AdMobInterstitial.isReady((cb)=>{
    if(!cb){
      AdMobInterstitial.requestAd();
    }
  });
    let selectedState = this.props.navigation.getParam("state", "")

    let url = "http://rtoinfo.dybydx.co/get_rto_office_district.php";

    let formdata = new FormData();

    formdata.append("state", selectedState);
   
    fetch(url, {
      method: "post",
      headers: {
        "Content-Type": "multipart/form-data"
      },
      body: formdata
    })
      .then(response => response.json())
      .then(responseJson => {
        
if(responseJson.status=="1"){
       
        this.setState({
          data:responseJson.rto_districts,
          completeData:responseJson.rto_districts,
           isLoading: false
         });

        }else{
          this.setState({
         
             isLoading: false
           });
        }

      })
      .catch(error => {
        console.error(error);
      });
  }
  _keyExtractor = (item, index) => item.name+""+index;

  _onPressItem = (item,isState,isRTO) => {
    if(isRTO){
      AdMobInterstitial.isReady((cb)=>{
        if(cb&&MyConst.isFullPage){
          AdMobInterstitial.showAd();
          AdMobInterstitial.requestAd();
        }
      });
      this.props.navigation.navigate("RTOInfo", {
        data: item
      });
    }
  };
  _renderItem = ({ item }) => (
    <StateRow item={item} onPressItem={this._onPressItem} isState={false} isRTO={true}/>
  );
  render() {
    let loader;
    if (this.state.isLoading)
      loader = (
        <View
          style={{ justifyContent: "center", width:'100%', height:'100%', position: "absolute" }}
        >
          <MyLoader text="Getting Cities.."/>
        </View>
      );
    return (
      <LinearGradient
        start={{ x: 0, y: 0 }}
        end={{ x: 0, y: 1 }}
        colors={["#FF9933", "white", "#138808"]}
        style={{ width: "100%", height: "100%" }}
      >
      <SafeAreaView style={styles.container}>
        {/* <View style={{ width: "100%", height: 40, backgroundColor: "gray" }} /> */}
        {loader}

        <View style={{flexDirection:"row",width,justifyContent:"center",alignItems:"center"}}>
        <TouchableOpacity
        style={{paddding: 10,}}
            onPress={() => {
              AdMobInterstitial.isReady((cb)=>{
                if(cb&&MyConst.isFullPage){
                  AdMobInterstitial.showAd();
                  AdMobInterstitial.requestAd();
                }
              });
              this.props.navigation.goBack();
            }}
          >
            <Image
              style={{
                width: 30,
                height: 30,
                resizeMode: "contain",
                tintColor: "white",
                margin:10,
             
               
              }}
              source={require("./assets/back.png")}
            />
          </TouchableOpacity>
          <TextInput
          
              style={{
                borderColor: "white",
                backgroundColor: "white",
                borderWidth: 1,
                marginTop:10,
               flex:1,
                padding:10,
                borderRadius: 5,
                marginLeft:10,
                marginRight: 10,
                fontSize: 20,
            
                textAlign: "center"
              }}
              value={this.state.dob}
              onChangeText={text =>  {
                let items = this.state.completeData.filter(function(item){
                  return item.district.includes(text) ;
               });
               if(text!="")
               this.setState({data:items});
               else{
                this.setState({data:this.state.completeData});
               }
              }
              }
              maxLength={15}
              placeholder="Search City"
            
            />
        </View>
       
        <FlatList
          data={this.state.data}
          extraData={this.state}
          keyExtractor={this._keyExtractor}
          renderItem={this._renderItem}
        />
        {MyConst.isAdEnable ? this.bannerAd() : <View />}
      </SafeAreaView>
      </LinearGradient>
    );
  }
}
export const { width, height } = Dimensions.get("window");
const styles = StyleSheet.create({
  container: {
    flex: 1,

   
  },
  upper: {
    color: "gray",

    alignSelf: "center",
    textAlign: "center"
  }
});
