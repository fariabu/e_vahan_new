import React, { PureComponent } from "react";
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  TouchableWithoutFeedback
} from "react-native";

export default class DLRow extends PureComponent {
  _onPress = () => {
    this.props.onPressItem(this.props.item);
  };
  getWidth(toCalculate) {
    return (toCalculate / 375) * width;
  }

  
  
  render() {
    return(
      <TouchableWithoutFeedback onPress={this._onPress}>
        <View
          style={{
            backgroundColor: "white",
            margin: 5,
            marginRight: 10,
            borderRadius: 5,
            
          }}
        >
          <View style={{ flexDirection: "column", padding: 10 }}>
            <Text style={{ fontSize: 20,fontWeight:"bold",color:"#FF9933" }}>{this.props.item.dl_no}</Text>
            <Text style={{ fontSize: 16,color:"#138808" }}>{this.props.item.name}</Text>
          </View>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}
export const { width, height } = Dimensions.get("window");
const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    backgroundColor: "#1693e5"
  },
  upper: {
    color: "gray",

    alignSelf: "center",
    textAlign: "center"
  },
  text: {
    fontSize: 15
  },
  colorAccent: { color: "#8dbb3c" }
});
