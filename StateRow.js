import React, { PureComponent } from "react";
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  TouchableWithoutFeedback
} from "react-native";

export default class StateRow extends PureComponent {
  _onPress = () => {
    this.props.onPressItem(this.props.item,this.props.isState,this.props.isRTO);
  };
  getWidth(toCalculate) {
    return (toCalculate / 375) * width;
  }

  _renderStateRTO(){
return(
<TouchableWithoutFeedback onPress={this._onPress}>
        <View
          style={{
            backgroundColor: "white",
            margin: 5,
            marginRight: 10,
            borderRadius: 5,
            shadowColor: "#000",
            shadowOffset: { width: 0, height: 2 },
            shadowOpacity: 0.8,

            elevation: 1
          }}
        >
          <View style={{ flexDirection: "column", padding: 10 }}>
            <Text style={{ fontSize: 20 }}>{this.props.item.state}</Text>
          </View>
        </View>
      </TouchableWithoutFeedback>
);
  }
  _renderDistrictRTO(){
    return(
      <TouchableWithoutFeedback onPress={this._onPress}>
        <View
          style={{
            backgroundColor: "white",
            margin: 5,
            marginRight: 10,
            borderRadius: 5,
            shadowColor: "#000",
            shadowOffset: { width: 0, height: 2 },
            shadowOpacity: 0.8,

            elevation: 1
          }}
        >
          <View style={{ flexDirection: "column", padding: 10 }}>
            <Text style={{ fontSize: 20}}>{this.props.item.district}</Text>
          </View>
        </View>
      </TouchableWithoutFeedback>
    );
      }
      _renderStatePetrol(){
        return(
          <TouchableWithoutFeedback onPress={this._onPress}>
        <View
          style={{
            backgroundColor: "white",
            margin: 5,
            marginRight: 10,
            borderRadius: 5,
            shadowColor: "#000",
            shadowOffset: { width: 0, height: 2 },
            shadowOpacity: 0.8,

            elevation: 1
          }}
        >
          <View style={{ flexDirection: "column", padding: 10 }}>
          
            <Text style={{ fontSize: 20,color:"black" }}>{this.props.item.cityState}</Text>
          </View>
        </View>
      </TouchableWithoutFeedback>
        );
          }
          _renderDisctrictPetrol(){
            return(
              <TouchableWithoutFeedback onPress={this._onPress}>
        <View
          style={{
            backgroundColor: "white",
            margin: 5,
            marginRight: 10,
            borderRadius: 5,
            shadowColor: "#000",
            shadowOffset: { width: 0, height: 2 },
            shadowOpacity: 0.8,

            elevation: 1
          }}
        >
          <View style={{ flexDirection: "column", padding: 10 }}>
            <Text style={{ fontSize: 20 }}>{this.props.item.state}</Text>
          </View>
        </View>
      </TouchableWithoutFeedback>
            );
              }
  render() {
    if(this.props.isState){
      if(this.props.isRTO)
      return this._renderStateRTO();
      else{
        return this._renderStatePetrol();
      }
    }else{
      if(this.props.isRTO)
      return this._renderDistrictRTO();
      else{
        return this._renderDisctrictPetrol();
      }
    }
  }
}
export const { width, height } = Dimensions.get("window");
const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    backgroundColor: "#1693e5"
  },
  upper: {
    color: "gray",

    alignSelf: "center",
    textAlign: "center"
  },
  text: {
    fontSize: 15
  },
  colorAccent: { color: "#8dbb3c" }
});
