/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from "react";
import {
  StyleSheet,
  Image,
  SafeAreaView,

  Platform,
  BackHandler,
  Dimensions,
  FlatList,
  TouchableOpacity,
  DeviceEventEmitter,
  ActivityIndicator,
  View,
  Text
} from "react-native";
import LinearGradient from "react-native-linear-gradient";
var SQLite = require("react-native-sqlite-storage");
import DateTimePicker from 'react-native-modal-datetime-picker';
import Geocoder from "react-native-geocoder";
import { TextInput } from "react-native-gesture-handler";
import MyLoader from "./MyLoader";
import VehicleRow from "./VehicleRow";
import DLRow from "./DLRow";
import {
  AdMobBanner,
  AdMobInterstitial,
  PublisherBanner,
  AdMobRewarded
} from "react-native-admob";
import MyConst from "./MyConst";
import MyBanner from "./MyBanner";
export default class DLInput extends Component {
  constructor(props) {
    super(props);
    this.db = SQLite.openDatabase(
      "RTO_INFO.db",
      "1.0",
      "RTO DB",
      200000,
      this.openCB,
      this.errorCB
    );
    // {dl_no":"UP6120110000200","dob":"04-09-1988","status":"ACTIVE","name":"SYED AHMAD  RAZA  RIZVI","date_of_issue":"07-Jan-2011","last_transaction_at":"ASST.RTO, GHAZIPUR","non_transport_from":"From: 07-Jan-2011","non_transport_to":"To: 06-Jan-2031","transport_from":"From: NA","transport_to":"To: NA","hazardous_valid_till":"NA","hill_valid_till":"NA","created_at":"2019-01-26 14:46:33","updated_at":"2019-01-26 14:46:33","source":"fresh"
    this.db.transaction(function(txn) {
      let qry =
        "CREATE TABLE IF NOT EXISTS DLInfo (dl_no TEXT PRIMARY KEY NOT NULL,dob Text(12),status Text(10),name VARCHAR(50),date_of_issue VARCHAR(50),last_transaction_at VARCHAR(50),non_transport_to VARCHAR(50),source VARCHAR(50))";
      txn.executeSql(qry, []);
    });
    this.deviceWidth = Dimensions.get("window").width;
    this.response = "";
    this.state = {
      isLoading: false,
      location: "",
      dob: "",
      isDateTimePickerVisible: false,
      vehicleNo: "",
      history:[]
    };
  }
  _showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });
 
  _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });
 
  _handleDatePicked = (date) => {
    console.log('A date has been picked: ', date);
    // let d = new Date();
    // d.getVarDate
    this.setState({dob:date.getDate()+"-"+(date.getMonth()+1)+"-"+date.getFullYear()});
    this._hideDateTimePicker();
  };
  refresh() {
if(this.state.vehicleNo==""||this.state.dob=="")
{
  alert("Pls fill all the fields");
  return;
}

this.db.transaction((tx) => {
  tx.executeSql('SELECT * FROM DLInfo where dl_no='+"'"+this.state.vehicleNo+"'", [], (tx, results) => {
      console.log("Query completed"+results.rows.length);
     console.log(results.rows.raw());
     if(results.rows.length>0){
      this.setState({
        data:results.rows.raw()[0],
         isLoading: false
       });
       this.props.navigation.navigate("DLInfo", {
        data: results.rows.raw()[0]
      });
      }else{
        this.setState({ isLoading: true });
   
    let formdata = new FormData();

    formdata.append("dl_no", this.state.vehicleNo);
    formdata.append("dob", this.state.dob);
    fetch(`http://rtoinfo.dybydx.co/find_dl.php`, {
      method: "post",
      headers: {
        "Content-Type": "multipart/form-data"
      },
      body: formdata
    })
      .then(response => response.json())
      .then(responseJson => {
        console.log(responseJson);
        let rcData = responseJson.dl;
        this.setState({ isLoading: false, vehicleNo: "" });
        if(responseJson.status=="1"){
          let query = "INSERT INTO DLInfo (dl_no,dob,status,name,date_of_issue,last_transaction_at,non_transport_to,source) VALUES('"+rcData.dl_no+"','"+rcData.dob+"','"+rcData.status+"','"+rcData.name+"','"+rcData.date_of_issue+"','"+rcData.last_transaction_at+"','"+rcData.non_transport_to+"','"+rcData.source+"');";
                
               
               
          this.db.executeSql(query);
          // dl_no TEXT PRIMARY KEY NOT NULL,dob Text(12),status Text(10),name VARCHAR(50),date_of_issue VARCHAR(50),last_transaction_at VARCHAR(50),non_transport_to VARCHAR(50),source VARCHAR(50)
        this.props.navigation.navigate("DLInfo", {
          data: rcData
        });
      }
      })
      .catch(error => {
        // console.error(error);
        alert("Data not found");
        this.setState({ isLoading: false });
      });
      }
      // Alternatively, you can use the non-standard raw method.
 
      /*
        let rows = results.rows.raw(); // shallow copy of rows Array
 
        rows.map(row => console.log(`Employee name: ${row.name}, Dept Name: ${row.deptName}`));
      */
    });
});


    
  }
  bannerAd() {
    return (
      <MyBanner/>
    );
  }
  handleBackButton = () => {
    DeviceEventEmitter.emit('Back',  {hello:"yes"});
    AdMobInterstitial.isReady((cb)=>{
      if(cb&&MyConst.isFullPage){
        AdMobInterstitial.showAd();
        AdMobInterstitial.requestAd();
      }
    });
    this.props.navigation.goBack();
    MyConst.CurrentScreen="Home";
    return true;
  };
  componentWillUnmount() {
    if (Platform.OS == "android")
    BackHandler.removeEventListener(
      "hardwareBackPress",
      this.handleBackButton
    );
  }
  componentDidMount() {
    if (Platform.OS == "android")
    BackHandler.addEventListener("hardwareBackPress", this.handleBackButton);
 
    // AdMobInterstitial.setAdUnitID(MyConst.FullPageID);
    AdMobInterstitial.isReady((cb)=>{
    if(!cb){
      AdMobInterstitial.requestAd();
    }
  });
    this.db.transaction((tx) => {
      tx.executeSql('SELECT * FROM DLInfo', [], (tx, results) => {
          console.log("Query completed"+results.rows.length);
         console.log(results.rows.raw());
         if(results.rows.length>0){
          this.setState({
            history : results.rows.raw().reverse(),
             isLoading: false
           });
           
          }
          
          // Alternatively, you can use the non-standard raw method.
     
          /*
            let rows = results.rows.raw(); // shallow copy of rows Array
     
            rows.map(row => console.log(`Employee name: ${row.name}, Dept Name: ${row.deptName}`));
          */
        });
    });
    

  }
  getWidth(toCalculate) {
    return (toCalculate / 375) * width;
  }
  _keyExtractor = (item, index) => item.name+""+index;

  _onPressItem = (item) => {
    console.log("row clicked");
    this.props.navigation.navigate("DLInfo", {
      data: item
    });
  };
  _renderItem = ({ item }) => (
    <DLRow item={item} onPressItem={this._onPressItem} />
  );
  render() {
    const gradient = `linear-gradient(-225deg, #231557 0%, #44107A 29%, #FF1361 67%, #FFF800 100%), repeating-linear-gradient(-115deg, transparent, transparent 20px, rgba(255,255,255,0.1) 20px, rgba(255,255,255,0.1) 40px), repeating-linear-gradient(115deg, transparent, transparent 20px, rgba(255,255,255,0.1) 20px, rgba(255,255,255,0.1) 40px)`;

    return (
      <LinearGradient
        start={{ x: 0, y: 0 }}
        end={{ x: 0, y: 1 }}
        colors={["#FF9933", "white", "#138808"]}
        style={{ width: "100%", height: "100%" }}
      >
        <SafeAreaView
          style={{
            backgroundColor: "#0004",
            width: "100%",
            height: "100%",
            shadowColor: "#000",
            shadowOffset: { width: 0, height: 2 },
            shadowOpacity: 0.8,

            elevation: 1
          }}
        >
         <DateTimePicker
          isVisible={this.state.isDateTimePickerVisible}
          onConfirm={this._handleDatePicked}
          minimumDate={new Date(1-1-2000)}
          onCancel={this._hideDateTimePicker}
        />
          <TouchableOpacity
          style={{paddding: 10,}}
            onPress={() => {
              AdMobInterstitial.isReady((cb)=>{
                if(cb&&MyConst.isFullPage){
                  AdMobInterstitial.showAd();
                  AdMobInterstitial.requestAd();
                }
              });
              DeviceEventEmitter.emit('Back',  {hello:"yes"});
              
              // MyConst.CurrentScreen="Home";
              this.props.navigation.goBack();
              MyConst.CurrentScreen="Home";
            }}
          >
            <Image
              style={{
                width: 30,
                height: 30,
                resizeMode: "contain",
                tintColor: "white",
                margin:10,
              }}
              source={require("./assets/back.png")}
            />
          </TouchableOpacity>
          <Image
            source={require("./assets/splash_rto.png")}
            style={{ width: "80%", height: 150, alignSelf: "center" }}
            resizeMode="contain"
          />
          <Text
            style={{
              color: "white",
              fontSize: 25,
              padding: 4,
              textAlign: "center"
            }}
          >
            Enter Your License Number
          </Text>
          <View
            style={{
              padding: 10,
              marginTop: 5,
              width: this.getWidth(275),
              borderRadius: 5,

              justifyContent: "center",
              flexDirection: "row",
            
              alignSelf: "center",
              backgroundColor: "white"
            }}
          >
            <View
              style={{
                position: "absolute",
                width: "100%",
                height: "100%",
                marginTop: 10,
                marginBottom: 10,
                justifyContent: "space-between",
                flexDirection: "row",
                alignItems: "center"
              }}
            >
            
            </View>
            <TextInput
              style={{
                borderColor: "white",
                borderWidth: 1,
                borderRadius: 5,
                fontSize: 25,
                padding: 4,
                textAlign: "center"
              }}
              value={this.state.vehicleNo}
              autoCapitalize="characters"
              onChangeText={text => this.setState({ vehicleNo: text })}
              maxLength={15}
              placeholder="TN7520138090907"
            />
          </View>
          <View style={{marginTop: 10,
              width: this.getWidth(275),
              justifyContent:"center",
              flexDirection: "row",
              alignSelf: "center",
              }}>
             <TouchableOpacity   onPress={this._showDateTimePicker}>
          <TextInput
          pointerEvents="none"
              style={{
                borderColor: "white",
                backgroundColor: "white",
                borderWidth: 1,
                padding:10,
                borderRadius: 5,
                fontSize: 25,
            
                textAlign: "center"
              }}

              editable={false}
              autoCapitalize="characters"
              value={this.state.dob}
              onChangeText={text => this.setState({ dob: text })}
              maxLength={15}
              placeholder="DATE OF BIRTH"
            
            />
           </TouchableOpacity>
          <TouchableOpacity
            style={{
              alignSelf: "center",
              width: 60,
            marginLeft:10,
              height: 60,
              borderRadius: 30,
              justifyContent: "center",
              backgroundColor: "black"
            }}
            
            onPress={() => {
              this.refresh();
            }}
          >
          
            <Image
              style={{ width: 30, height: 30, alignSelf: "center" }}
              resizeMode="contain"
              source={require("./assets/search.png")}
            />
          </TouchableOpacity>
          </View>
          <Text
            style={{
              color: "white",
              fontSize: 25,
              marginTop:10,
              padding: 4,
             
            }}
          >
            Recent Search
          </Text>
         
          <FlatList
          
          data={this.state.history}
          extraData={this.state}
          keyExtractor={this._keyExtractor}
          renderItem={this._renderItem}
        />
          {MyConst.isAdEnable ? this.bannerAd() : <View />}
           {this.state.isLoading ? <MyLoader text="Getting DL Info"/> : <View />}
          
          {/* <Text style={{color:"white"}}>{this.state.location}</Text> */}
        </SafeAreaView>
      </LinearGradient>
    );
  }
}
export const { width, height } = Dimensions.get("window");
const styles = StyleSheet.create({
  container: {
    flex: 1,

    backgroundColor: "black"
  },
  upper: {
    color: "gray",

    alignSelf: "center",
    textAlign: "center"
  }
});
