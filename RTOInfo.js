/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from "react";
import {
  StyleSheet,
  Image,
  SafeAreaView,

  Dimensions,
  Platform,BackHandler,
  TouchableOpacity,
  ActivityIndicator,
  View,
  Text
} from "react-native";
import LinearGradient from "react-native-linear-gradient";
import Geocoder from "react-native-geocoder";
import {
  AdMobBanner,
  AdMobInterstitial,
  PublisherBanner,
  AdMobRewarded
} from "react-native-admob";
import MyConst from "./MyConst";
import { TextInput, ScrollView } from "react-native-gesture-handler";
import MyBanner from "./MyBanner";
const DateDiff = require("date-diff");
export default class RTOInfo extends Component {
  constructor(props) {
    super(props);

    this.deviceWidth = Dimensions.get("window").width;
    this.response = "";
    this.state = {
      location: "",
      data: this.props.navigation.getParam("data", "")
    
    };
  }
  bannerAd() {
    return (
      <MyBanner/>
    );
  }
  handleBackButton = () => {
    // DeviceEventEmitter.emit('Back',  {hello:"yes"});
    this.props.navigation.goBack();
    AdMobInterstitial.isReady((cb)=>{
      if(cb&&MyConst.isFullPage){
        AdMobInterstitial.showAd();
        AdMobInterstitial.requestAd();
      }
    });
    MyConst.CurrentScreen="RTo";
    // this.props.navigation.goBack();
    return true;
  };
  componentWillUnmount() {
    if (Platform.OS == "android")
    
    BackHandler.removeEventListener(
      "hardwareBackPress",
      this.handleBackButton
    );
  }
  componentDidMount() {
    if (Platform.OS == "android")
    BackHandler.addEventListener("hardwareBackPress", this.handleBackButton);
    // AdMobInterstitial.setAdUnitID(MyConst.FullPageID);
    AdMobInterstitial.isReady((cb)=>{
    if(!cb){
      AdMobInterstitial.requestAd();
    }
  });
    this.setState(
      { data: this.props.navigation.getParam("data", "")},
      function() {}
    );
  }
  getWidth(toCalculate) {
    return (toCalculate / 375) * width;
  }

  render() {
    const gradient = `linear-gradient(-225deg, #231557 0%, #44107A 29%, #FF1361 67%, #FFF800 100%), repeating-linear-gradient(-115deg, transparent, transparent 20px, rgba(255,255,255,0.1) 20px, rgba(255,255,255,0.1) 40px), repeating-linear-gradient(115deg, transparent, transparent 20px, rgba(255,255,255,0.1) 20px, rgba(255,255,255,0.1) 40px)`;
   
    return (
      <LinearGradient
        start={{ x: 0, y: 0 }}
        end={{ x: 0, y: 1 }}
        colors={["#FF9933", "white", "#138808"]}
        style={{ width: "100%", height: "100%" }}
      >
        <SafeAreaView
          style={{
            backgroundColor: "#0004",
            width: "100%",
            height: "100%",
            shadowColor: "#000",
            shadowOffset: { width: 0, height: 2 },
            shadowOpacity: 0.8,

            elevation: 1
          }}
        >
          <TouchableOpacity
          style={{paddding: 10,}}
            onPress={() => {
              AdMobInterstitial.isReady((cb)=>{
                if(cb&&MyConst.isFullPage){
                  AdMobInterstitial.showAd();
                  AdMobInterstitial.requestAd();
                }
              });
              this.props.navigation.goBack();
            }}
          >
            <Image
              style={{
                width: 30,
                height: 30,
                resizeMode: "contain",
                tintColor: "white",
                margin:10,
              }}
              source={require("./assets/back.png")}
            />
          </TouchableOpacity>
          <ScrollView>
            <View>
              <Image
                source={require("./assets/rto_office.png")}
                style={{ width: "80%", height: 200, alignSelf: "center", }}
                resizeMode="contain"
              />
              {MyConst.isAdEnable ? this.bannerAd() : <View />}
              <View
                style={{
                  width: this.getWidth(340),
                  borderRadius: 5,
                  alignSelf: "center",
                  flexDirection: "column",
                  backgroundColor: "white",
                  marginTop:10
                }}
              >
                <Text style={styles.headingText}>Code</Text>

                <Text style={styles.mainText}>
                  {this.state.data.code}
                </Text>
              </View>
              <View
                style={{
                  width: this.getWidth(340),
                  borderRadius: 5,
                  alignSelf: "center",
                  flexDirection: "column",
                  marginTop:10,
                  backgroundColor: "white"
                }}
              >
                <Text style={styles.headingText}>District</Text>

                <Text style={styles.mainText}>
                  {this.state.data.district}
                </Text>
              </View>
              <View
                style={{
                  marginTop:10,
                  width: this.getWidth(340),
                  borderRadius: 5,
                  alignSelf: "center",
                  flexDirection: "column",
                  backgroundColor: "white"
                }}
              >
                <Text style={styles.headingText}>Address</Text>

                <Text style={styles.mainText}>
                  {this.state.data.address}
                </Text>
              </View>
              <View
                style={{
                  marginTop:10,
                  width: this.getWidth(340),
                  borderRadius: 5,
                  alignSelf: "center",
                  flexDirection: "column",
                  backgroundColor: "white"
                }}
              >
                <Text style={styles.headingText}>Contact</Text>

                <Text style={styles.mainText}>
                  {this.state.data.phone}
                </Text>
              </View>
              <View
                style={{
                  marginTop:10,
                  width: this.getWidth(340),
                  borderRadius: 5,
                  alignSelf: "center",
                  flexDirection: "column",
                  backgroundColor: "white"
                }}
              >
                <Text style={styles.headingText}>Website</Text>

                <Text style={styles.mainText}>
                  {this.state.data.website}
                </Text>
              </View>
              
            </View>
          </ScrollView>
        </SafeAreaView>
      </LinearGradient>
    );
  }
}
export const { width, height } = Dimensions.get("window");
const styles = StyleSheet.create({
  container: {
    flex: 1,

    backgroundColor: "black"
  },
  bg: {},
  headingText: {
    color: "#FF9933",
    fontWeight: "bold",
    padding: 10,
    fontSize: 14
  },
  mainText: {
    color: "#138808",
    paddingLeft: 10,
    paddingRight: 10,
    width: "100%",
    paddingBottom: 10,

    fontSize: 16
  },
  upper: {
    color: "gray",

    alignSelf: "center",
    textAlign: "center"
  }
});
