/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from "react";
import {
  StyleSheet,
  Image,
  SafeAreaView,

  Dimensions,
  FlatList,
  ActivityIndicator,
  View,
  Text
} from "react-native";
import HomeRow from "./HomeRow";
var SQLite = require('react-native-sqlite-storage')
export default class Home extends Component {
  constructor(props) {
    super(props);
    this.db = SQLite.openDatabase("nexogenDemo.db", "1.0", "Test DB", 200000, this.openCB, this.errorCB);
    this.db.transaction(function (txn) {
    
      txn.executeSql('CREATE TABLE IF NOT EXISTS TestUsers (user_id INTEGER PRIMARY KEY NOT NULL, name VARCHAR(30), email Text(30))', []);
    });
    this.deviceWidth = Dimensions.get("window").width;
    this.response = "";
    this.state = {
      isLoading: true,
      data: []
    };
  }
  errorCB(err) {
    console.log("SQL Error: " + err);
  }
   
  successCB() {
    console.log("SQL executed fine");
  }
   
  openCB() {
    console.log("Database OPENED");
  }
  componentDidMount() {

    // this.watchID = navigator.geolocation.watchPosition((position) => {
    //   // Create the object to update this.state.mapRegion through the onRegionChange function
    //   let region = {
    //     latitude:       position.coords.latitude,
    //     longitude:      position.coords.longitude,
    //     latitudeDelta:  0.00922*1.5,
    //     longitudeDelta: 0.00421*1.5
    //   }
    //   this.onRegionChange(region, region.latitude, region.longitude);
    // }, (error)=>console.log(error));

    let url = "https://jsonplaceholder.typicode.com/users";
    fetch(url, {
      method: "GET"
    })
      .then(response => response.json())
      .then(responseJson => {
        

        for(var i=0;i<responseJson.length;i++){
          // console.log(`Employee name: ${responseJson[i].id}, Dept Name: ${responseJson[i].name}`);
          let query = "INSERT INTO TestUsers (user_id, name,email) VALUES ("+responseJson[i].id+",'"+responseJson[i].name+"','"+responseJson[i].email+"');";
          console.log(query);
                      this.db.executeSql(query);
                      
                  }
        

        this.db.transaction((tx) => {
          tx.executeSql('SELECT * FROM TestUsers', [], (tx, results) => {
              console.log("Query completed"+results.rows.length);
             
             
              this.setState({
                data:results.rows.raw(),
                 isLoading: false
               });
              // Alternatively, you can use the non-standard raw method.
         
              /*
                let rows = results.rows.raw(); // shallow copy of rows Array
         
                rows.map(row => console.log(`Employee name: ${row.name}, Dept Name: ${row.deptName}`));
              */
            });
        });
        

      })
      .catch(error => {
        console.error(error);
      });
  }
  _keyExtractor = (item, index) => item.user_id+""+index;

  _onPressItem = (id) => {
    console.log("row clicked");
  };
  _renderItem = ({ item }) => (
    <HomeRow item={item} onPressItem={this._onPressItem} />
  );
  render() {
    let loader;
    if (this.state.isLoading)
      loader = (
        <View
          style={{ justifyContent: "center", width:'100%', height:'100%', position: "absolute" }}
        >
          <ActivityIndicator size="large" color="white" style={{alignSelf:'center'}}/>
        </View>
      );
    return (
      <SafeAreaView style={styles.container}>
        <View style={{ width: "100%", height: 40, backgroundColor: "gray" }} />
        {loader}
        <FlatList
          data={this.state.data}
          extraData={this.state}
          keyExtractor={this._keyExtractor}
          renderItem={this._renderItem}
        />
      </SafeAreaView>
    );
  }
}
export const { width, height } = Dimensions.get("window");
const styles = StyleSheet.create({
  container: {
    flex: 1,

    backgroundColor: "maroon"
  },
  upper: {
    color: "gray",

    alignSelf: "center",
    textAlign: "center"
  }
});
