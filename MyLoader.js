/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from "react";
import {
  StyleSheet,
  Image,
  SafeAreaView,

  Dimensions,
  TouchableOpacity,
  ActivityIndicator,
  View,
  Text
} from "react-native";
import LinearGradient from "react-native-linear-gradient";
import BouncingPreloader from "react-native-bouncing-preloader";
import Geocoder from "react-native-geocoder";
import { TextInput } from "react-native-gesture-handler";
export default class MyLoader extends Component {
  constructor(props) {
    super(props);

    this.deviceWidth = Dimensions.get("window").width;
    this.response = "";
    this.state = {
      
    };
  }

  
  renderLoader() {
    return (
      <View
        style={{
          width,height,
          position: "absolute",
          justifyContent: "center",
          backgroundColor:"#0004",
          alignItems: "center"
        }}
      >
      <View style={{width:"80%",padding:10,alignSelf:"center",backgroundColor:"white",borderRadius:10}}>
        <Image
          source={require("./assets/myLoader.gif")}
          alignSelf= "center"
          borderRadius={10}
          style={{ width, height: 100,  }}
          resizeMode="contain"
        />
        <Text style={{alignSelf:"center",fontSize:18,fontWeight:"bold"}}>
          Getting Vehicle Info
        </Text>
        </View>
      </View>
    );
  }
  componentDidMount() {}
  getWidth(toCalculate) {
    return (toCalculate / 375) * width;
  }

  render() {
    
   
      return(
<View
        style={{
          width,height,
          position: "absolute",
          justifyContent: "center",
          backgroundColor:"#0004",
          alignItems: "center"
        }}
      >
      <View style={{width:"80%",padding:10,alignSelf:"center",backgroundColor:"white",borderRadius:10}}>
        <Image
          source={require("./assets/myLoader.gif")}
          alignSelf= "center"
          borderRadius={10}
          style={{ width, height: 100,  }}
          resizeMode="contain"
        />
        <Text style={{alignSelf:"center",fontSize:18,fontWeight:"bold"}}>
         {this.props.text}
        </Text>
        </View>
      </View>

      );
    
  }
}
export const { width, height } = Dimensions.get("window");
const styles = StyleSheet.create({
  container: {
    flex: 1,

    backgroundColor: "black"
  },
  upper: {
    color: "gray",

    alignSelf: "center",
    textAlign: "center"
  }
});
