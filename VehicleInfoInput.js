/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from "react";
import {
  StyleSheet,
  Image,
  SafeAreaView,
  

  FlatList,
  Dimensions,
  DeviceEventEmitter,
  TouchableOpacity,
  ActivityIndicator,
  Platform,
  BackHandler,
  View,
  Text
} from "react-native";
import LinearGradient from "react-native-linear-gradient";
var SQLite = require("react-native-sqlite-storage");
import BouncingPreloader from "react-native-bouncing-preloader";
import Geocoder from "react-native-geocoder";
import { TextInput } from "react-native-gesture-handler";
import MyLoader from "./MyLoader";
import VehicleRow from "./VehicleRow";
import {
  AdMobBanner,
  AdMobInterstitial,
  PublisherBanner,
  AdMobRewarded
} from "react-native-admob";
import MyConst from "./MyConst";
import MyBanner from "./MyBanner";
import { fetch_rc_details } from "./rc_details";
export default class VehicleInfoInput extends Component {
  constructor(props) {
    super(props);
    this.db = SQLite.openDatabase(
      "RTO_INFO.db",
      "1.0",
      "RTO DB",
      200000,
      this.openCB,
      this.errorCB
    );


      

    this.db.transaction(function(txn) {
      let qry =
        "CREATE TABLE IF NOT EXISTS VehicleInfo (vehicle_no TEXT PRIMARY KEY NOT NULL,reg_prefix Text(6),reg_suffix Text(6),reg_authority VARCHAR(50),reg_date VARCHAR(50),chassis_no VARCHAR(50),engine_no VARCHAR(50),owner_name VARCHAR(50),vehicle_class VARCHAR(50),fuel_type VARCHAR(50),maker_model VARCHAR(50),fitness_upto VARCHAR(50),insurance_upto VARCHAR(50),fuel_norms VARCHAR(50),road_tax_upto VARCHAR(50),noc VARCHAR(30))";
      txn.executeSql(qry, []);
    });
    this.deviceWidth = Dimensions.get("window").width;
    this.response = "";
    this.state = {
      isLoading: false,
      location: "",
      vehicleNo: "",
      history:[]
    };
  }

  refresh() {
    if (this.state.vehicleNo == "") {
      alert("Please Enter Vehicle No");
      return;
    }

    this.db.transaction((tx) => {
      tx.executeSql('SELECT * FROM VehicleInfo where vehicle_no='+"'"+this.state.vehicleNo+"'", [], (tx, results) => {
          console.log("Query completed"+results.rows.length);
         console.log(results.rows.raw());
         if(results.rows.length>0){
          this.setState({
            data:results.rows.raw()[0],
             isLoading: false
           });
           this.props.navigation.navigate("VehicleInfo", {
            data: results.rows.raw()[0]
          });
          }else{
            this.setState({ isLoading: true });
            let s1 = this.state.vehicleNo.substring(0, this.state.vehicleNo.length - 4);
            let s2 = this.state.vehicleNo.substring(
              this.state.vehicleNo.length - 4,
              this.state.vehicleNo.length
            );
            let formdata = new FormData();
        
            formdata.append("prefix", s1);
            formdata.append("suffix", s2);

            fetch_rc_details(this.state.vehicleNo).then(responseJson=>{
              console.log("opopopo");

              console.log(responseJson);
              console.log("responseJson");
              this.setState({ isLoading: false, vehicleNo: "" });
             
              let rcData = responseJson;
              let vNo = rcData.reg_prefix+""+rcData.reg_suffix;
             
                          
                      

            
                let query = "INSERT INTO VehicleInfo (vehicle_no,reg_prefix,reg_suffix,reg_authority,reg_date,chassis_no,engine_no,owner_name,vehicle_class,fuel_type,maker_model,fitness_upto,insurance_upto,fuel_norms,road_tax_upto,noc) VALUES('"+vNo+"','"+rcData.reg_prefix+"','"+rcData.reg_suffix+"','"+rcData.reg_authority+"','"+rcData.reg_date+"','"+rcData.chassis_no+"','"+rcData.engine_no+"','"+rcData.owner_name+"','"+rcData.vehicle_class+"','"+rcData.fuel_type+"','"+rcData.maker_model+"','"+rcData.fitness_upto+"','"+rcData.insurance_upto+"','"+rcData.fuel_norms+"','"+rcData.road_tax_upto+"','"+rcData.noc+"');";
              
             
             
                this.db.executeSql(query);
                this.props.navigation.navigate("VehicleInfo", {
                  data: rcData
                });
              
              
              
              
            });
            
            // fetch(`http://rtoinfo.dybydx.co/find_rc.php`, {
            //   method: "post",
            //   headers: {
            //     "Content-Type": "multipart/form-data"
            //   },
            //   body: formdata
            // })
            //   .then(response => response.json())
            //   .then(responseJson => {
            //     this.setState({ isLoading: false, vehicleNo: "" });
            //     let rcData = responseJson.rc;
            //     let vNo = rcData.reg_prefix+""+rcData.reg_suffix;
               
                            
                        

            //     if (responseJson.status == "1") {
            //       let query = "INSERT INTO VehicleInfo (vehicle_no,reg_prefix,reg_suffix,reg_authority,reg_date,chassis_no,engine_no,owner_name,vehicle_class,fuel_type,maker_model,fitness_upto,insurance_upto,fuel_norms,road_tax_upto,noc) VALUES('"+vNo+"','"+rcData.reg_prefix+"','"+rcData.reg_suffix+"','"+rcData.reg_authority+"','"+rcData.reg_date+"','"+rcData.chassis_no+"','"+rcData.engine_no+"','"+rcData.owner_name+"','"+rcData.vehicle_class+"','"+rcData.fuel_type+"','"+rcData.maker_model+"','"+rcData.fitness_upto+"','"+rcData.insurance_upto+"','"+rcData.fuel_norms+"','"+rcData.road_tax_upto+"','"+rcData.noc+"');";
                
               
               
            //       this.db.executeSql(query);
            //       this.props.navigation.navigate("VehicleInfo", {
            //         data: rcData
            //       });
                
                
                
            //     } else {
            //       alert("Data not found");
            //     }
            //   })
            //   .catch(error => {
            //     // console.error(error);
            //     alert("Data not found");
            //     this.setState({ isLoading: false });
            //   });
          }
          // Alternatively, you can use the non-standard raw method.
     
          /*
            let rows = results.rows.raw(); // shallow copy of rows Array
     
            rows.map(row => console.log(`Employee name: ${row.name}, Dept Name: ${row.deptName}`));
          */
        });
    });
    

   
  }
  renderLoader() {
    return (
      <View
        style={{
          width,
          height,
          position: "absolute",
          justifyContent: "center",
          backgroundColor: "#0004",
          alignItems: "center"
        }}
      >
        <View
          style={{
            width: "80%",
            padding: 10,
            alignSelf: "center",
            backgroundColor: "white",
            borderRadius: 10
          }}
        >
          <Image
            source={require("./assets/myLoader.gif")}
            alignSelf="center"
            borderRadius={10}
            style={{ width, height: 100 }}
            resizeMode="contain"
          />
          <Text
            style={{ alignSelf: "center", fontSize: 18, fontWeight: "bold" }}
          >
            Getting Vehicle Info
          </Text>
        </View>
      </View>
    );
  }
  bannerAd() {
    return (
      <MyBanner/>
    );
  }
  handleBackButton = () => {
    DeviceEventEmitter.emit('Back',  {hello:"yes"});
    AdMobInterstitial.isReady((cb)=>{
      if(cb&&MyConst.isFullPage){
        AdMobInterstitial.showAd();
        AdMobInterstitial.requestAd();
      }
    });
    this.props.navigation.goBack();

    MyConst.CurrentScreen="Home";
    // this.props.navigation.goBack();
    return true;
  };
  componentWillUnmount() {
    if (Platform.OS == "android")
    BackHandler.removeEventListener(
      "hardwareBackPress",
      this.handleBackButton
    );
  }
  componentDidMount() {
    if (Platform.OS == "android")
    BackHandler.addEventListener("hardwareBackPress", this.handleBackButton);
    // AdMobInterstitial.setAdUnitID(MyConst.FullPageID);
    AdMobInterstitial.isReady((cb)=>{
    if(!cb){
      AdMobInterstitial.requestAd();
    }
  });
    this.db.transaction((tx) => {
      tx.executeSql('SELECT * FROM VehicleInfo', [], (tx, results) => {
          console.log("Query completed"+results.rows.length);
         console.log(results.rows.raw());
         if(results.rows.length>0){
          this.setState({
            history : results.rows.raw().reverse(),
             isLoading: false
           });
           
          }
          
          // Alternatively, you can use the non-standard raw method.
     
          /*
            let rows = results.rows.raw(); // shallow copy of rows Array
     
            rows.map(row => console.log(`Employee name: ${row.name}, Dept Name: ${row.deptName}`));
          */
        });
    });
    

  }
  getWidth(toCalculate) {
    return (toCalculate / 375) * width;
  }
  _keyExtractor = (item, index) => item.name+""+index;

  _onPressItem = (item) => {
    console.log("row clicked");
    AdMobInterstitial.isReady((cb)=>{
      if(cb&&MyConst.isFullPage){
        AdMobInterstitial.showAd();
        AdMobInterstitial.requestAd();
      }
    });
    this.props.navigation.navigate("VehicleInfo", {
      data: item
    });
  };
  _renderItem = ({ item }) => (
    <VehicleRow item={item} onPressItem={this._onPressItem} />
  );
  render() {
    const gradient = `linear-gradient(-225deg, #231557 0%, #44107A 29%, #FF1361 67%, #FFF800 100%), repeating-linear-gradient(-115deg, transparent, transparent 20px, rgba(255,255,255,0.1) 20px, rgba(255,255,255,0.1) 40px), repeating-linear-gradient(115deg, transparent, transparent 20px, rgba(255,255,255,0.1) 20px, rgba(255,255,255,0.1) 40px)`;

    return (
      <LinearGradient
        start={{ x: 0, y: 0 }}
        end={{ x: 0, y: 1 }}
        colors={["#FF9933", "white", "#138808"]}
        style={{ width: "100%", height: "100%" }}
      >
        <SafeAreaView
          style={{
            backgroundColor: "#0002",
            width: "100%",
            height: "100%",
            shadowColor: "#000",
            shadowOffset: { width: 0, height: 2 },
            shadowOpacity: 0.8,

            elevation: 1
          }}
        >
        <View style={{flex:1}}>
          <TouchableOpacity
          style={{paddding: 10,}}
            onPress={() => {
              AdMobInterstitial.isReady((cb)=>{
                if(cb&&MyConst.isFullPage){
                  AdMobInterstitial.showAd();
                  AdMobInterstitial.requestAd();
                }
              });
              DeviceEventEmitter.emit('Back',  {hello:"yes"});
              this.props.navigation.goBack();
              MyConst.CurrentScreen="Home";
            }}
          >
            <Image
              style={{
                width: 30,
                height: 30,
                margin:10,
                resizeMode: "contain",
                tintColor: "white",
                
              
              }}
              source={require("./assets/back.png")}
            />
          </TouchableOpacity>
          <Image
            source={require("./assets/splash_rto.png")}
            style={{ width: "80%", height: 150, alignSelf: "center",marginTop:10 }}
            resizeMode="contain"
          />
          <Text
            style={{
              color: "white",
              fontSize: 25,
              padding: 4,
              textAlign: "center"
            }}
          >
            Enter Vehicle Number
          </Text>
          <View
            style={{
              padding: 10,
              marginTop: 5,
              width: this.getWidth(275),
              borderRadius: 10,
              justifyContent: "center",
              flexDirection: "row",
              alignSelf: "center",
              backgroundColor: "white"
            }}
          >
            <View
              style={{
                position: "absolute",
                width: "100%",
                height: "100%",
                marginTop: 10,
                marginBottom: 10,
                justifyContent: "space-between",
                flexDirection: "row",
                alignItems: "center"
              }}
            >
              <View
                style={{
                  alignSelf: "center",
                  width: 10,
                  height: 10,
                  borderRadius: 5,
                  backgroundColor: "black"
                }}
              />
              <View
                style={{
                  width: 10,
                  height: 10,
                  borderRadius: 5,
                  backgroundColor: "black"
                }}
              />
            </View>
            <TextInput
              style={{
                borderColor: "white",
                borderWidth: 1,
                borderRadius: 5,
                fontSize: 25,
                padding: 4,
                textAlign: "center"
              }}
              value={this.state.vehicleNo}
              autoCapitalize="characters"
              onChangeText={text => this.setState({ vehicleNo: text.toUpperCase() })}
              maxLength={10}
              placeholder="DL2CXXXXXX"
            />
          </View>
          <TouchableOpacity
            style={{
              alignSelf: "center",
              width: 60,
              marginTop: 20,
              height: 60,
              borderRadius: 30,
              justifyContent: "center",
              backgroundColor: "black"
            }}
            onPress={() => {
              this.refresh();
            }}
          >
            <Image
              style={{ width: 30, height: 30, alignSelf: "center" }}
              resizeMode="contain"
              source={require("./assets/search.png")}
            />
          </TouchableOpacity>
          <Text
            style={{
              color: "white",
              fontSize: 25,
              marginTop:10,
              padding: 4,
             
            }}
          >
            Recent Search
          </Text>
         
          <FlatList
          
          data={this.state.history}
          extraData={this.state}
          keyExtractor={this._keyExtractor}
          renderItem={this._renderItem}
        />

          {this.state.isLoading ? (
            <MyLoader text="Getting Vehicle Info" />
          ) : (
            <View />
          )}
          </View>
           {MyConst.isAdEnable ? this.bannerAd() : <View />}
        </SafeAreaView>

      </LinearGradient>
    );
  }
}
export const { width, height } = Dimensions.get("window");
const styles = StyleSheet.create({
  container: {
    flex: 1,

    backgroundColor: "black"
  },
  upper: {
    color: "gray",

    alignSelf: "center",
    textAlign: "center"
  }
});
